(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; (setq package-initialize-at-startup nil)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; for elpa?
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(require 'use-package)
(setq use-package-always-ensure t)

(require 'cl)

(fset 'yes-or-no-p 'y-or-n-p)

(setq-default indent-tabs-mode nil)
(setq create-lockfiles nil)
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Set up load path
(setq dotfiles-dir (file-name-directory (or (buffer-file-name) load-file-name))
      site-lisp-dir (expand-file-name "site-lisp" dotfiles-dir))
(add-to-list 'load-path site-lisp-dir)

(load "00-funs")
(load "00-macros")
(load "05-look-and-feel")
(load-file (expand-file-name "~/.emacs.d/site-lisp/10-packages.el"))

(load "private" t)

(setq custom-file "~/.emacs.d/site-lisp/custom.el")
(load custom-file)

(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'prog-mode-hook 'flyspell-prog-mode)
(add-hook 'prog-mode-hook 'column-number-mode)

(setq gnus-init-file "~/.emacs.d/site-lisp/setup-gnus.el")
(setq vc-follow-symlinks t)

;; why doesn't this work inside a load-ed file?
;; (leaf org
;;   :ensure t
;;   :require t
;;   :bind (("C-c a" . org-agenda)
;;          ("C-c c" . org-capture))
;;   :setq ((org-agenda-files . '("~/org/"))
;;          (org-default-notes-file . "~/org/inbox.org")
;;          (org-capture-templates . '(("t" "Task" entry (file "") "* TODO %?\n%u")))
;;          (org-todo-keywords . '((sequence "TODO(t)" "WAIT(w@/!)" "|" "DONE(d)" "SOMEDAY(s)")
;;                                 ))
;;          (org-return-follows-link . t)
;;          ))

;; (leaf scala-mode  
;;   :package t)

(use-package autorevert
  :ensure nil ; builtin
  :diminish auto-revert-mode
  :config
  (setq auto-revert-check-vc-info t)
  (global-auto-revert-mode t))

;; (leaf autorevert
;;   :doc "revert buffers when files change on disk"
;;   :tag "builtin"
;;   ;;:custom ((auto-revert-interval . 0.1))
;;   :global-minor-mode global-auto-revert-mode)

;; (leaf org-journal
;;   :package t
;;   :bind (("C-c n j" . org-journal-new-entry))
;;   :setq (
;;          (org-journal-dir . "~/org")
;;          (org-journal-date-prefix . "#+TITLE: ")
;;          (org-journal-file-format . "%Y-%m-%d.org")
;;          (org-journal-date-format . "%A, %d %B %Y")
;;          ;;(org-journal-file-type . monthly)

;;          )
;;   )

;; (leaf org-roam
;;   :package t
;;   :bind ((:org-roam-mode-map
;;               (("C-c n l" . org-roam)
;;                ("C-c n f" . org-roam-find-file)
;;                ;; ("C-c n j" . org-roam-jump-to-index)
;;                ("C-c n b" . org-roam-switch-to-buffer)
;;                ("C-c n g" . org-roam-graph)))
;;          (:org-mode-map
;;               (("C-c n i" . org-roam-insert))))
;;   :setq (
;;          (org-roam-directory . "~/org")
;;          )
;;   :global-minor-mode org-roam-mode
;;   )

;; (leaf deft
;;   :package t
;;   :after org
;;   :bind (("C-c n d" . deft))
;;   :setq (
;;          (deft-recursive . t)
;;          (deft-use-filter-string-for-filename . t)
;;          (deft-default-extension . "org")
;;          (deft-directory . "~/org")
;;          ))

;; (leaf org-roam-protocol
;;   :after org-roam
;;   :require t)

;; (leaf server
;;   :if window-system
;;   :tag "builtin"
;;   :hook (after-init-hook server-start))
(use-package server
  :ensure nil ; builtin
  :if window-system
  :config
  (unless (server-running-p)
    (server-start)))

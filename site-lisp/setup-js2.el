(setq js2-basic-offset 4
      js2-skip-preprocessor-directives t ;; treat shebangs like comments
      ;; js2-auto-indent-p t
      ;; js2-cleanup-whitespace t
      ;; js2-enter-indents-newline t
      ;; js2-indent-on-enter-key t
      js2-bounce-indent-p t
      js2-highlight-level 3)
(setq-default js2-global-externs '("angular"))

(add-hook 'js2-mode-hook
          (lambda ()
            (define-key js2-mode-map "\C-ci" 'js-doc-insert-function-doc)
            (define-key js2-mode-map "@" 'js-doc-insert-tag)))

(provide 'setup-js2)

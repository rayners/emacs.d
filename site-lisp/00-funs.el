(defun ensure-installed (package &optional after)
  (if (not (package-installed-p package))
      (package-install package)
    )
  (if after
      (funcall after)))

(autoload 'auth-source-search "auth-source" "auth-source" t)
(defun rayners/authinfo-password-for (host user)
  "Return the password for a given host/user/port combination"
  (let ((secret (plist-get (nth 0 (auth-source-search :host host :user user :type 'netrc)) :secret)))
    (if secret
        (funcall secret))))

(defmacro p (form)
  "Output pretty `macroexpand-1'ed form of given FORM."
  `(progn
     (pp (macroexpand-1 ',form))
     nil))

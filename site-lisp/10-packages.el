(use-package paren
  :init
  (show-paren-mode t))

(use-package paradox
  :config (paradox-enable))

(use-package exec-path-from-shell
  :config
  (when (memq window-system '(mac ns))
    (exec-path-from-shell-initialize)))

(use-package swiper
  :bind ("C-s" . swiper)
  :config (ivy-mode 1)
  :demand t)

(use-package js2-mode
  ; :mode "\\.js\\'"
  :interpreter "node"
  :config
  (add-hook 'js2-mode-hook #'add-node-modules-path)
  :init (require 'setup-js2))

(use-package js2-refactor
  :config
  (js2r-add-keybindings-with-prefix "C-c C-m")
  (add-hook 'js2-mode-hook #'js2-refactor-mode)
  )

(use-package js-mode
  :ensure nil ; it's builtin
  :after (js2-mode)
  :mode "\\.jsx?\\'"
  :hook (js-mode-hook . js2-minor-mode)
  )

(use-package add-node-modules-path
  ;; :after (js2-mode)
  ;; :config (add-hook 'js2-mode-hook #'add-node-modules-path)
  ;; :hook (js2-mode-hook . add-node-modules-path)
  :hook (web-mode-hook . add-node-modules-path)
)

;; (use-package tern
;;   :config
;;   (add-hook 'js2-mode-hook (lambda ()
;;                              (tern-mode t)
;;                              ))
;;   )

;; (use-package js-doc)
(use-package markdown-mode)

(use-package projectile
  :init
  (projectile-mode +1)
  (setq projectile-keymap-prefix (kbd "C-c p")) ;; temp for counsel-projectile
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (setq projectile-completion-system 'ivy)
  (projectile-register-project-type 'npm '("package.json")
                                    :compile "npm install"
                                    :test "npm test"
                                    :run "npm start"
                                    :test-suffix ".spec")
  )

(use-package counsel-projectile
  :requires projectile
  :init
  (counsel-projectile-mode)
  )

 ;; (leaf org
 ;;  :ensure t
 ;;  :require t
 ;;  :bind (("C-c a" . org-agenda)
 ;;         ("C-c c" . org-capture))
 ;;  :setq ((org-agenda-files . '("~/org/"))
 ;;         (org-default-notes-file . "~/org/inbox.org")
 ;;         (org-capture-templates . '(("t" "Task" entry (file "") "* TODO %?\n%u")))
 ;;         (org-todo-keywords . '((sequence "TODO(t)" "WAIT(w@/!)" "|" "DONE(d)" "SOMEDAY(s)")
 ;;                                ))
 ;;         (org-return-follows-link . t)
 ;;         ))

(use-package org
  :bind (("C-c a" . org-agenda)
         ("C-c c" . org-capture))
  :init
  (setq org-agenda-files '("~/org" "~/org/beorg")
        org-default-notes-file "~/org/beorg/inbox.org"
        org-return-follows-link t)
  )

(use-package org-projectile
  :after (org projectile)
  :bind ("C-c n p" . org-projectile-project-todo-completing-read)
  :config
  (org-projectile-per-project)
  (setq org-projectile-per-project-filepath "project.org")
  (setq org-agenda-files (append org-agenda-files (org-projectile-todo-files))))

;; (use-package org-projectile
;;   :bind ("C-c n p" . org-projectile-project-todo-completing-read)
;;   :config
;;   (progn    
;;     ;; (org-projectile-per-project)
;;     (setq org-projectile-projects-file (expand-file-name "~/iCloud/org/projects.org"))
;;     ;; (setq org-projectile-per-project-filepath "todo.org")
;;     (setq org-agenda-files (append org-agenda-files (org-projectile-todo-files)))))

(use-package magit
  :init (require 'setup-magit)
  :bind ("C-x C-z" . magit-status))

(use-package hl-todo
  :hook (prog-mode-hook . hl-todo-mode))

(use-package magit-todos
  :requires magit
  :init
  (magit-todos-mode)
  )

;; (use-package fic-mode
;;   :diminish fic-mode
;;   :init
;;   (add-hook 'prog-mode-hook 'turn-on-fic-mode))

(use-package flycheck
  :diminish "✈✓"
  :init
  (setq flycheck-eslintrc ".eslintrc")
  (setq flycheck-disabled-checkers '(javascript-jshint))
  (setq flycheck-checkers '(javascript-eslint))
  (add-hook 'prog-mode-hook 'flycheck-mode)
  ;; (flycheck-add-mode `typescript-tslint 'web-mode)
  )

(use-package osx-dictionary
  :if (eq system-type 'darwin)
  )

(use-package yaml-mode)

(eval-after-load 'rcirc #'(require 'setup-rcirc))

;; (use-package web-mode
;;   :mode (("\\.hbs\\'" . web-mode)
;;          ("\\.tsx\\'" . web-mode))
;;   :config
;;   (setq web-mode-markup-indent-offset 4)
;;   :init
;;   (add-hook 'web-mode-hook
;;             (lambda ()
;;               {when (string-equal "tsx" (file-name-extension buffer-file-name))
;;                 (setup-tide-mode)))
;;   )

(use-package multiple-cursors)

;(leaf org :leaf-defer nil :ensure t :require t :setq (org-agenda-files . '("~/org/inbox.org")))

;; (leaf org
;;   :require t
;;   :ensure t
;;   ;; :bind (("C-c a" . org-agenda)
;;   ;;        ("C-c c" . org-capture))
;;   :setq ((org-agenda-files . '("~/org/inbox.org"))))
;; (leaf org-agenda
;;   :require t
;;   :setq ((org-agenda-files . '("~/org/inbox.org"))))

(use-package feature-mode)
(use-package groovy-mode)

;; (use-package company)
(use-package typescript-mode
  :ensure t
  :hook ((typescript-mode . (lambda ()
                              (add-node-modules-path)
                              (setup-tide-mode)
                              (subword-mode)
                              (company-mode))))
  :bind ((:map typescript-mode-map
               ("C-c C-t" . tide-documentation-at-point)))
)

(use-package flycheck
  :ensure t
  :config
  (add-hook 'typescript-mode-hook 'flycheck-mode))

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (flycheck-mode +1)
  (prettier-js-mode)
  (setq typescript-indent-level 2)
  (flycheck-add-next-checker 'typescript-tide 'javascript-eslint)
  (setq tide-completion-detailed t))
  ;; (setq flycheck-check-syntax-automatically '(save mode-enabled))
  ;; (flycheck-add-next-checker 'typescript-tide '(t . typescript-tslint) 'append)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  ;; (company-mode +1))

(use-package company
  :ensure t
  :config
  (setq company-show-numbers t)
  (setq company-tooltip-align-annotations t)
  ;; invert the navigation direction if the the completion popup-isearch-match
  ;; is displayed on top (happens near the bottom of windows)
  (setq company-tooltip-flip-when-above t)
  (global-company-mode))

(use-package tide
  :ensure t
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
                                        ;(before-safe . tide-format-before-save)
         ))

(use-package company-quickhelp
  :ensure t
  :init
  (company-quickhelp-mode 1)
  (use-package pos-tip
    :ensure t))

(use-package web-mode
  :ensure t
  :mode (("\\.html?\\'" . web-mode)
         ("\\.tsx\\'" . web-mode))
;         ("\\.jsx\\'" . web-mode))
  :config
  (setq web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-block-padding 2
        web-mode-comment-style 2

        web-mode-enable-css-colorization t
        web-mode-enable-auto-pairing t
        web-mode-enable-comment-keywords t
        web-mode-enable-current-element-highlight t
        )
  (add-hook 'web-mode-hook
            (lambda ()
              (when (string-equal "tsx" (file-name-extension buffer-file-name))
		(setup-tide-mode))))
  ;; enable typescript-tslint checker
  (flycheck-add-mode 'typescript-tslint 'web-mode))

(use-package prettier-js
  :ensure t
  :config
  ;; (setq prettier-js-command "/usr/local/bin/prettier")
  (add-hook 'web-mode-hook
            (lambda ()
              (when (string-equal "tsx" (file-name-extension buffer-file-name))
                (prettier-js-mode)))))

(use-package notmuch
  :ensure t
  :bind (("C-c m m" . rayners-notmuch)
         :map notmuch-search-mode-map
         ("D" . (lambda ()
                  "Mark message as trash"
                  (interactive)
                  (notmuch-search-tag '("-inbox" "+trash"))
                  (notmuch-search-next-thread))))
  ;; :custom-face
  ;; (notmuch-search-unread-face ((t (:foreground nil :weight bold :slant italic))))
  :config
  (defun rayners-notmuch ()
    (interactive)
    (delete-other-windows)
    (notmuch))
  
  (setq notmuch-archive-tags '("-inbox" "+archive")
        notmuch-search-oldest-first nil
        notmuch-saved-searches '((:name "inbox" :query "tag:inbox" :key "i")
                                 (:name "github" :query "tag:github")
                                 (:name "jira" :query "tag:jira")
                                 (:name "unread" :query "tag:unread" :key "u")
                                 (:name "flagged" :query "tag:flagged" :key "f")
                                 (:name "sent" :query "tag:sent" :key "t")
                                 (:name "drafts" :query "tag:draft" :key "d")
                                 (:name "all mail" :query "*" :key "a"))

        )
  )

(use-package rainbow-delimiters
  :hook (prog-mode-hook . rainbow-delimiters-mode))

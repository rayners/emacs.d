(setq magit-set-upstream-on-push t)
(add-hook 'git-commit-mode-hook (lambda nil
                                  (flyspell-mode 1)))

(setq magit-repository-directories '(("~/Code" . 3)
                                     ("~/.emacs.d" . 1))
      magit-completing-read-function 'ivy-completing-read)

(setq magit-last-seen-setup-instructions "1.4.0")

(provide 'setup-magit)

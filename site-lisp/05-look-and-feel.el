(menu-bar-mode -1)
(tool-bar-mode -1)

(setq inhibit-startup-screen t)

(if window-system
    (progn
      (add-to-list 'default-frame-alist '(alpha 95 75))
                                        ;(add-to-list 'default-frame-alist '(font . "Anonymous Pro-11"))
      ;;(add-to-list 'default-frame-alist '(font . "Source Code Pro-10"))
      ;; (add-to-list 'default-frame-alist '(font . "Oxygen Mono-10"))
      ;;(add-to-list 'default-frame-alist '(font . "Hack-10"))
      (add-to-list 'default-frame-alist '(width . 132))
      (add-to-list 'default-frame-alist '(height . 60))
      (set-fontset-font t 'unicode "Apple Color Emoji" nil 'prepend)
      ;;(add-to-list 'default-frame-alist '(font . "GoMono Nerd Font Mono-10"))
      ;;(add-to-list 'default-frame-alist '(font . "Fira Code-12"))
      (add-to-list 'default-frame-alist '(font . "Hack Nerd Font Mono-12"))
      ))

(use-package zerodark-theme
  :disabled
  :config
  (load-theme 'zerodark t)
  (zerodark-setup-modeline-format))

(use-package one-themes
  :disabled
  :config
  (load-theme 'one-dark t))

(use-package zeno-theme
  :disabled
  :config
  (load-theme 'zeno t))

(use-package doom-theme
  :disabled
  :config
  ;; (load-theme 'doom-tomorrow-night t))
  (load-theme 'doom-one t))
  ;; (load-theme 'doom-dark+ t))

(use-package nimbus-theme
  :disabled
  :config
  (load-theme 'nimbus t))

(use-package color-theme-sanityinc-tomorrow
  :disabled
  :config
  (load-theme 'sanityinc-tomorrow-bright t))

(use-package telephone-line
  :disabled
  :config
  (defface rayners/mode-line-red '((t (:background "red3" :inherit telephone-line-evil))) "")
  (setq telephone-line-faces
        '((evil . telephone-line-modal-face)
          (modal . telephone-line-modal-face)
          (ryo . telephone-line-ryo-modal-face)
          (red . (rayners/mode-line-red . rayners/mode-line-red))
          (accent telephone-line-accent-active . telephone-line-accent-inactive)
          (nil mode-line . mode-line-inactive)))
  (setq telephone-line-lhs
        '((red  . (telephone-line-vc-segment))
          (accent . (telephone-line-erc-modified-channels-segment telephone-line-process-segment telephone-line-projectile-segment))
          (nil    . (telephone-line-buffer-segment)))
        )
  (setq telephone-line-rhs
        '((nil . (telephone-line-misc-info-segment))
          (accent . (telephone-line-flycheck-segment telephone-line-major-mode-segment))
          (red . (telephone-line-airline-position-segment)))
        )
  (telephone-line-mode t))

(use-package smart-mode-line
  :disabled
  :config
  (sml/setup))

(use-package spaceline-config
  :disabled
  :ensure spaceline
  :config
  (setq powerline-default-separator 'contour)
  (spaceline-spacemacs-theme)
  )

(use-package doom-modeline
  :hook (after-init . doom-modeline-mode))

(use-package spacemacs-theme
  :disabled
  :config
  (load-theme 'spacemacs t))

(use-package zenburn-theme
  :disabled
  :config
  (load-theme 'zenburn t))

(use-package brutalist-theme
  :disabled
  :config
  (load-theme 'brutalist-dark t))

(use-package weyland-yutani-theme
  :disabled
  :config
  (load-theme 'weyland-yutani t))

(use-package solo-jazz-theme
  :disabled
  :config
  (load-theme 'solo-jazz t))

(use-package vscode-dark-plus-theme
  :config
  (load-theme 'vscode-dark-plus t))


;; packages specifically for irc
(ensure-installed 'rcirc-color (lambda ()
                                 (require 'rcirc-color)))

(setq rcirc-default-nick "rayners"
      rcirc-default-full-name "David Raynes")

(setq rcirc-server-alist
      `(
        ("irc.freenode.net"
         :channels ("#emacs" "#5by5" "#angularjs" "##javascript" "#node.js")
         :encryption tls
         :port 6697
         )
        )
      )

(setq rcirc-authinfo
      `(("freenode" nickserv "rayners" ,(rayners/authinfo-password-for "irc.freenode.net" "rayners")))
      )

;; if I need to use a server that's stricter about line endings...
;; (defadvice rcirc-send-string (before add-carriage-return activate)
;;   (ad-set-arg 1 (concat (ad-get-arg 1) "\r")))

(setq rcirc-prompt "[%n @ %t]> ")
(provide 'setup-rcirc)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#282c34" "#ff6c6b" "#98be65" "#ECBE7B" "#51afef" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(custom-safe-themes
   '("0b7e79de7c8d857d53c8df7449c61deb2035cb276372ea4ad65fe7e6f1b046ca" "c8e076f0e2df414c02fdb46b09b735628e73c73f72f9d78392edf99de7d86977" "bd2fa4ae674367ca2d2b895d1bda7bf734c20ed7ed303c1c8062f488ba028b3a" "5ed25f51c2ed06fc63ada02d3af8ed860d62707e96efc826f4a88fd511f45a1d" "7aaee3a00f6eb16836f5b28bdccde9e1079654060d26ce4b8f49b56689c51904" "a339f231e63aab2a17740e5b3965469e8c0b85eccdfb1f9dbd58a30bdad8562b" "008dec0f293f1fc5c1125dfd74125078b32fbe3a6e6348589b071989364bdbbd" "8ffdc8c66ceeaf7921f4510a70d808f01b303e6b4d177c947b442e80d4228678" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "d71aabbbd692b54b6263bfe016607f93553ea214bc1435d17de98894a5c3a086" "e1ef2d5b8091f4953fe17b4ca3dd143d476c106e221d92ded38614266cea3c8b" "e14e8e4ecae1b4e0983367c6ab7437720f10b85865549b9ea329be2c2eaa9e80" "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "99ea831ca79a916f1bd789de366b639d09811501e8c092c85b2cb7d697777f93" "be9645aaa8c11f76a10bcf36aaf83f54f4587ced1b9b679b55639c87404e2499" "fa3bdd59ea708164e7821574822ab82a3c51e262d419df941f26d64d015c90ee" "e1ecb0536abec692b5a5e845067d75273fe36f24d01210bf0aa5842f2a7e029f" "7b50dc95a32cadd584bda3f40577e135c392cd7fb286a468ba4236787d295f4b" "c19e5291471680e72d8bd98f8d6e84f781754a9e8fc089536cda3f0b7c3550e3" "edea0b35681cb05d1cffe47f7eae912aa8a930fa330f8c4aeb032118a5d0aabf" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "b73a23e836b3122637563ad37ae8c7533121c2ac2c8f7c87b381dd7322714cd0" default))
 '(fci-rule-color "#5B6268")
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#51afef"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#98be65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#3f444a"))
 '(objed-cursor-color "#ff6c6b")
 '(package-selected-packages
   '(vscode-dark-plus-theme org-gcal slack forge rainbow-mode solo-jazz-theme protobuf-mode weyland-yutani-theme haml-mode deft org-roam-protocol org-roam org-journal scala-mode nvm leaf-keywords prism brutalist-theme zenburn-theme moe-theme planet-theme ledger-mode doom-modeline nimbus-theme color-theme-sanityinc-tomorrow doom-themes poet-theme org-jira sql-indent notmuch prettier-js groovy-mode zeno-theme htmlize magit-todos js2-refactor telephone-line one-themes ox-gfm counsel-projectile yaml-mode warm-night-theme rcirc-color rainbow-delimiters projectile markdown-mode magit less-css-mode json-mode js-doc highlight-thing grandshell-theme flycheck fic-mode feature-mode exec-path-from-shell ecukes atom-dark-theme ag))
 '(paradox-github-token t)
 '(pdf-view-midnight-colors (cons "#bbc2cf" "#282c34"))
 '(rustic-ansi-faces
   ["#282c34" "#ff6c6b" "#98be65" "#ECBE7B" "#51afef" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(safe-local-variable-values
   '((js2-basic-offset . 2)
     (flycheck-jshintrc . "~/Code/hackathon-dashboard/.jshintrc")
     (js2-additional-externs quote
                             ("angular"))))
 '(vc-annotate-background "#282c34")
 '(vc-annotate-color-map
   (list
    (cons 20 "#98be65")
    (cons 40 "#b4be6c")
    (cons 60 "#d0be73")
    (cons 80 "#ECBE7B")
    (cons 100 "#e6ab6a")
    (cons 120 "#e09859")
    (cons 140 "#da8548")
    (cons 160 "#d38079")
    (cons 180 "#cc7cab")
    (cons 200 "#c678dd")
    (cons 220 "#d974b7")
    (cons 240 "#ec7091")
    (cons 260 "#ff6c6b")
    (cons 280 "#cf6162")
    (cons 300 "#9f585a")
    (cons 320 "#6f4e52")
    (cons 340 "#5B6268")
    (cons 360 "#5B6268")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
